class_name Constants
extends Reference

enum lazer_tiers {
	LARGE,
	MEDIUM,
	SMALL,
	TINY,
};
var LAZER_TIERS = LAZER_TIERS;
