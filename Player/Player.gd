extends KinematicBody2D

var lazer_large = preload("res://Weapon/LazerLarge.tscn")
var lazer_tiny = preload("res://Weapon/LazerTiny.tscn")

export(float) var move_speed = 200.0;

func _physics_process(delta):
	var mouse_pos : Vector2 = get_global_mouse_position();
	$Sprite.look_at(mouse_pos);

	var v = Vector2(0, 0);

	if (Input.is_action_pressed('up')):
		v.y -= 1;
	if (Input.is_action_pressed('down')):
		v.y += 1;
	if (Input.is_action_pressed('left')):
		v.x -= 1;
	if (Input.is_action_pressed('right')):
		v.x += 1;

	move_and_slide(v.normalized() * delta * move_speed * 100);
	
func _shoot():
	var lazer = lazer_tiny.instance()#lazer_large.instance()
	lazer.init({"target": get_global_mouse_position(), "position": self.position})
	get_parent().add_child(lazer)
	
func _input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed:
		_shoot()
