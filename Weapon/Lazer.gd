extends KinematicBody2D

var screen_size
export var speed = 800.0;
var v = Vector2(1, 0);

func init(options = {}):
	if (options.has(speed)): 
		speed = options.speed;
	self.position = options.position
	v = (options.target - self.position).normalized()
	self.position += v * 20
	self.look_at(options.target)

func _process(delta):
	var velocity = v * speed
	position += velocity * delta
	
func _ready():
	screen_size = get_viewport_rect().size
