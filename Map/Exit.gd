extends AnimatedSprite


var curr_frame = 0;

func open():
	play();


func _on_Exit_frame_changed():
	var diff = 3 if (curr_frame) else 2;

	if (curr_frame < 7):
		$Top.position += Vector2(diff * -1, 0);
		$Bottom.position += Vector2(diff, 0);
		curr_frame += 1;
	else:
		stop();
